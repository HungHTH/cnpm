import 'package:flutter/material.dart';
import 'package:rive/rive.dart';

import '../../notifi/notifi_page.dart';

class Notifi extends StatelessWidget {
  const Notifi({super.key,});


  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: GestureDetector(
        onTap:()=>{ Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => const notifiPage(),
          ),
        )},
        child: Container(
          margin: const EdgeInsets.only(left: 12),
          height: 40,
          width: 40,
          decoration: const BoxDecoration(
            color: Colors.white,
            shape: BoxShape.circle,
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                offset: Offset(0, 3),
                blurRadius: 8,
              ),
            ],
          ),
          child: const Icon(Icons.notifications_none,color: Colors.black87,)
        ),
      ),
    );
  }
}
