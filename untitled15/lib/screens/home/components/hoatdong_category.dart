import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'hoatdong.dart';

class hoatdongCategory extends StatelessWidget {
  const hoatdongCategory({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(20),
          child: Text(
            "Hoạt động gần đây",
            style: Theme.of(context).textTheme.headlineSmall!.copyWith(
                color: Colors.black, fontWeight: FontWeight.bold),
          ),
        ),
        const Padding(
          padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
          child: HoatDong(
            title: "Hoạt động 1",
          ),
        ),
      ],
    );
  }
}
