import 'package:flutter/material.dart';

import '../../bonus/bonus_page.dart';
import '../../even/even_page.dart';
import '../../gia_pha/gia_pha_page.dart';
import '../../money/money_page.dart';
import 'course_card.dart';

class Danhmuc_category extends StatelessWidget {
  const Danhmuc_category({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 20, bottom: 10),
          child: Text(
            "Danh mục ",
            style: Theme.of(context).textTheme.headlineSmall!.copyWith(
                color: Colors.black, fontWeight: FontWeight.bold),
          ),
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 12),
                    child: CourseCard(
                      title:"Gia phả",
                      press: (){Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>  GiaPha(),
                        ),
                      );},
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: CourseCard(
                      title:"Sự kiện",
                      press: (){Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>  evenPage(),
                        ),
                      );},
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: CourseCard(
                      title:"Quỹ họ",
                      press: (){Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>  money_page(),
                        ),
                      );},
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: CourseCard(
                      title:"Khen Thưởng",
                      press: (){Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => BonusPage(),
                        ),
                      );},
                    ),
                  ),
                ]
            ),
          ),
        ),
      ],
    );
  }
}
