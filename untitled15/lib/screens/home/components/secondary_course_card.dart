import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class SuKien extends StatelessWidget {
  const SuKien({
    Key? key,
    required this.title,
    this.iconsSrc = "assets/icons/ios.svg",
    this.colorl = const Color(0xFF7553F6),
  }) : super(key: key);

  final String title, iconsSrc;
  final Color colorl;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          height: MediaQuery.of(context).size.width * 0.3,
          width: MediaQuery.of(context).size.width * 0.3,
          padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 20),
          decoration: BoxDecoration(
              color: colorl,
              borderRadius: const BorderRadius.all(Radius.circular(20))),
          child: SvgPicture.asset(iconsSrc)

        ),
        Padding(
          padding: const EdgeInsets.only(left: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: const [
              SizedBox(
                height: 50,
                width: 210,
                child: Text(
                  "Tên sự kiện 1 - Tên sự kiện 1  - Tên sự kiện 1",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                  ),
                  maxLines: 2,
                ),
              ),
              SizedBox(
                height: 30,
                child: Text(
                  "Địa điểm",
                  style: TextStyle(
                    color: Colors.black54,
                    fontSize: 16,
                  ),
                ),
              ),
              Text(
                "Thời gian",
                style: TextStyle(
                  color: Colors.black54,
                  fontSize: 16,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
