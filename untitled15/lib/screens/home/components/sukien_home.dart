import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:untitled15/screens/home/components/secondary_course_card.dart';

import '../../../model/course.dart';

class sukienHome extends StatelessWidget {
  const sukienHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
          Padding(
            padding: const EdgeInsets.all(20),
            child: Text(
              "Sự kiện sắp diễn ra",
              style: Theme.of(context).textTheme.headlineSmall!.copyWith(
                  color: Colors.black, fontWeight: FontWeight.bold),
            ),
          ),
          ...recentCourses
              .map((course) => Padding(
            padding: const EdgeInsets.only(
                left: 20, right: 20, bottom: 20),
            child: SuKien(
              title: course.title,
              iconsSrc: course.iconSrc,
              colorl: course.color,
            ),
          ))
              .toList(),
        ],
    );
  }
}
