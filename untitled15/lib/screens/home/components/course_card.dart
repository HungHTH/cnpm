import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../gia_pha/gia_pha_page.dart';

class CourseCard extends StatelessWidget {
  const CourseCard({
    Key? key,
    required this.title,
    this.color = const Color(0xFF7553F6),
    this.iconSrc = "assets/icons/ios.svg", required this.press,
  }) : super(key: key);

  final String title, iconSrc;
  final Color color;
  final VoidCallback press;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: press,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 24),
            height: 80,
            width: 80,
            decoration: BoxDecoration(
              color: color,
              borderRadius: BorderRadius.all(Radius.circular(30)),
            ),
            child: SvgPicture.asset(iconSrc),
          ),
          Text(
            title,
            style: Theme.of(context)
                .textTheme
                .titleMedium!
                .copyWith(color: Colors.black),
          )
        ],
      ),
    );
  }
}
