import 'package:flutter/material.dart';
import 'package:untitled15/screens/bonus/bonus_page.dart';
import 'package:untitled15/screens/even/even_page.dart';
import 'package:untitled15/screens/gia_pha/gia_pha_page.dart';
import 'package:untitled15/screens/money/money_page.dart';

import '../../model/course.dart';
import '../entryPoint/components/notifications.dart';
import 'components/course_card.dart';
import 'components/danhmuc_category.dart';
import 'components/hoatdong.dart';
import 'components/hoatdong_category.dart';
import 'components/secondary_course_card.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [
        SafeArea(
          bottom: false,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 40),
                Padding(
                  padding: const EdgeInsets.all(20),
                  child: Text(
                    "Thăng Long University",
                    style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                        color: Colors.black, fontWeight: FontWeight.bold),
                  ),
                ),
                const SizedBox(height: 20),
                const Danhmuc_category(),
                const hoatdongCategory(),
                Padding(
                  padding: const EdgeInsets.all(20),
                  child: Text(
                    "Sự kiện sắp diễn ra",
                    style: Theme.of(context).textTheme.headlineSmall!.copyWith(
                        color: Colors.black, fontWeight: FontWeight.bold),
                  ),
                ),
                ...recentCourses
                    .map((course) => Padding(
                          padding: const EdgeInsets.only(
                              left: 20, right: 20, bottom: 20),
                          child: SuKien(
                            title: course.title,
                            iconsSrc: course.iconSrc,
                            colorl: course.color,
                          ),
                        ))
                    .toList(),
                const SizedBox(
                  height: 60,
                ),
              ],
            ),
          ),
        ),
        const Positioned(
          right: 15,
          top: 15,
          child: Notifi(),
        )
      ]),
    );
  }
}
