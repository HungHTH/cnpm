import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Line extends StatelessWidget {
  final int? size;
  final int count;
  const Line({Key? key, required this.size, required this.count}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double line1 = MediaQuery.of(context).size.width*0.6;
    double line2 = MediaQuery.of(context).size.width*0.2;
    return size==1 ? Column(
      children: [
        Container(
          height: 40,
          width: 1,
          color: Colors.black87,
        ),
        Container(
          height: 1,
          width: line1,
          color: Colors.black87,
        ),
        Row(
          children: [
            Container(
              margin: EdgeInsets.only(left: (MediaQuery.of(context).size.width-line1)/2),
              height: 40,
              width: 1,
              color: Colors.black87,
            ),
            Container(
              margin: EdgeInsets.only(left: (MediaQuery.of(context).size.width)-(MediaQuery.of(context).size.width-line1)-2),
              height: 40,
              width: 1,
              color: Colors.black87,
            ),
          ],
        )
      ],
    ): size==2?Column(
      children: [
        Container(
          height: 30,
          width: 1,
          color: Colors.black87,
        ),
        Container(
          height: 1,
          width: line2,
          color: Colors.black87,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(left: (MediaQuery.of(context).size.width-line1)/2),
              height: 30,
              width: 1,
              color: Colors.black87,
            ),

            Container(
              margin: EdgeInsets.only(left: (MediaQuery.of(context).size.width)-(MediaQuery.of(context).size.width-line2)-2),
              height: 30,
              width: 1,
              color: Colors.black87,
            ),
          ],
        )
      ],
    ):Column();
  }
}
