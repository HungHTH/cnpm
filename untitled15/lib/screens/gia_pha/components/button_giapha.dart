import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class button_giapha extends StatelessWidget {
  final String link;
  final String kei;
  final int size;
  const button_giapha({
    Key? key, required this.link, required this.kei, required this.size,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: FirebaseFirestore.instance
          .collection(link)
          .snapshots(),
      builder: (ctx, streamSnapshot) {
        if (streamSnapshot.connectionState == ConnectionState.waiting) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        final documents = streamSnapshot.data;
        return size==1?SizedBox(
          height: 50,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: documents?.docs.length,
            itemBuilder: (ctx, index) => Container(
              width: 80,
              margin: EdgeInsets.all(8),
              decoration: BoxDecoration(
                  border: Border.all(
                    width: 1, //                   <--- border width here
                  ),
                  borderRadius: const BorderRadius.all(Radius.circular(5))),
              child: Center(child: Text(documents?.docs[index][kei])),
            ),
          ),
        ):SizedBox(
          height: 100,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: documents?.docs.length,
            itemBuilder: (ctx, index) => Container(
              width: 40,
              margin: EdgeInsets.all(8),
              decoration: BoxDecoration(
                  border: Border.all(
                    width: 1, //                   <--- border width here
                  ),
                  borderRadius: const BorderRadius.all(Radius.circular(5))),
              child: Center(child: Text(documents?.docs[index][kei])),
            ),
          ),
        );
      },
    );
  }
}
