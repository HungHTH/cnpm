import 'package:flutter/material.dart';

class MyAppSpace extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, c) {
        return Column(
          children: [
            Flexible(
              child: Container(
                width: double.infinity,
                child: Image.network(
                  'https://datamnguyen.vn/uploads/pictures/607670ba3573304e1ee23968/content_mau-nha-mo-dep-.jpg',
                  fit: BoxFit.fill,
                ),
              ),
            ),
            const Padding(
              padding: EdgeInsets.all(12.5),
              child: Text(
                'Phả đồ dòng họ XXX',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 26.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}