import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:untitled15/screens/gia_pha/components/line_giapha.dart';

import '../../constants.dart';
import 'components/button_giapha.dart';

// class GiaPha extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//
//     return Scaffold(
//       body: Row(
//         children: [
//           SizedBox(
//             height: 200,
//             width: 100,
//             child: StreamBuilder(
//               stream: FirebaseFirestore.instance
//                   .collection('giapha/W2ygBNHkc4GBCeQjvLpq/TH1')
//                   .snapshots(),
//               builder: (ctx, streamSnapshot) {
//                 if (streamSnapshot.connectionState == ConnectionState.waiting) {
//                   return Center(
//                     child: CircularProgressIndicator(),
//                   );
//                 }
//                 final documents = streamSnapshot.data;
//                 return ListView.builder(
//                   itemCount: documents?.docs.length,
//                   itemBuilder: (ctx, index) => Container(
//                     padding: EdgeInsets.all(8),
//                     child: Text(documents?.docs[index]['N1']),
//                   ),
//                 );
//               },
//             ),
//           ),
//           SizedBox(
//             height: 200,
//             width: 100,
//             child: StreamBuilder(
//               stream: FirebaseFirestore.instance
//                   .collection('giapha/W2ygBNHkc4GBCeQjvLpq/TH2')
//                   .snapshots(),
//               builder: (ctx, streamSnapshot) {
//                 if (streamSnapshot.connectionState == ConnectionState.waiting) {
//                   return Center(
//                     child: CircularProgressIndicator(),
//                   );
//                 }
//                 final documents = streamSnapshot.data;
//                 return ListView.builder(
//                   itemCount: documents?.docs.length,
//                   itemBuilder: (ctx, index) => Container(
//                     padding: EdgeInsets.all(8),
//                     child: Text(documents?.docs[index]['text']),
//                   ),
//                 );
//               },
//             ),
//           ),
//           SizedBox(
//             height: 200,
//             width: 100,
//             child: StreamBuilder(
//               stream: FirebaseFirestore.instance
//                   .collection('giapha/W2ygBNHkc4GBCeQjvLpq/TH3')
//                   .snapshots(),
//               builder: (ctx, streamSnapshot) {
//                 if (streamSnapshot.connectionState == ConnectionState.waiting) {
//                   return Center(
//                     child: CircularProgressIndicator(),
//                   );
//                 }
//                 final documents = streamSnapshot.data;
//                 return ListView.builder(
//                   itemCount: documents?.docs.length,
//                   itemBuilder: (ctx, index) => Container(
//                     padding: EdgeInsets.all(8),
//                     child: Text(documents?.docs[index]['text']),
//                   ),
//                 );
//               },
//             ),
//           ),
//         ],
//       ),
//       // floatingActionButton: FloatingActionButton(
//       //   child: Icon(Icons.add),
//       //   onPressed: () {
//       //     FirebaseFirestore.instance
//       //         .collection('cnpm/r6jysXENNANtfRWydPKT/message')
//       //         .add({'text': 'This was added by clicking the button!'});
//       //   },
//       // ),
//     );
//   }
// }
//

class GiaPha extends StatelessWidget {
  const GiaPha({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: OrientationBuilder(
        builder: (context, orientation) {
          if (orientation == Orientation.portrait) {
            return Center(child: Text("xoay ngang màn hình"),);
          } else {
            return Center(
              child: Container(
                height: 400,
                color: Colors.black,
                child: Scaffold(
                  body: Column(
                          children:  [
                            button_giapha(link: 'giapha/W2ygBNHkc4GBCeQjvLpq/TH1', kei: 'text', size: 1,),
                            Container(
                              height: 10,
                              width: 1,
                              color: Colors.black,
                            ),
                            Container(
                              height: 1,
                              width: 50,
                              color: Colors.black,
                            ),
                            button_giapha(link: 'giapha/W2ygBNHkc4GBCeQjvLpq/TH2', kei: 'text', size: 1,),
                            button_giapha(link: 'giapha/W2ygBNHkc4GBCeQjvLpq/TH3', kei: 'text', size: 1,),
                            button_giapha(link: 'giapha/W2ygBNHkc4GBCeQjvLpq/TH4', kei: 'text', size: 2,),
                            button_giapha(link: 'giapha/W2ygBNHkc4GBCeQjvLpq/TH5', kei: 'text', size: 2,),

                    ],
                  ),
                )
              ),
            );
          }
        },
      ),
    );
  }
}

