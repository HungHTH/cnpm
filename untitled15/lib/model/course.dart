import 'package:flutter/material.dart' show Color;

class Course {
  final String title, description, iconSrc;
  final Color color;

  Course({
    required this.title,
    this.description = 'Build and animate an iOS app from scratch',
    this.iconSrc = "assets/icons/ios.svg",
    this.color = const Color(0xFF7553F6),
  });

}

final List<Course> courses = [
  Course(
    title: "Gia phả",
  ),
  Course(
    title: "Sự kiện",
  ),
  Course(
    title: "Quỹ họ",
  ),
  Course(
    title: "Khen thưởng",
    iconSrc: "assets/icons/code.svg",
    color: const Color(0xFF80A4FF),
  ),
];

final List<Course> recentCourses = [
  Course(title: "Sự kiện 1"),
  Course(
    title: "Sự kiện 2",
    color: const Color(0xFF9CC5FF),
    iconSrc: "assets/icons/code.svg",
  ),
  Course(title: "Sự kiện 3"),
  Course(
    title: "Sự kiện 4",
    color: const Color(0xFF9CC5FF),
    iconSrc: "assets/icons/code.svg",
  ),
];
